#!/usr/bin/perl -w

use strict;
use warnings;

use Switch;
use Getopt::Long;
use POSIX;
use Text::CSV_XS qw( csv );
use Data::Dumper;  # Utilisé pour le débug

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Désactiver les warnings sur les fonctions "expérimentales" (qui marchent très bien).
no if ($] >= 5.018), 'warnings' => 'experimental';


############################
# Traitement des arguments #
############################

# Lecture des arguments
my $config_rules = '';
my $config_verbose = 0;
my $config_debug = 0;
my $config_quiet = 0;
my $config_xml = 0;
my $config_header = 0;
GetOptions(
  'rules|r=s'=>\$config_rules,
  'quiet|q'=>\$config_quiet,
  'verbose|v'=>\$config_verbose,
  'debug|d'=>\$config_debug,
  'xml|x'=>\$config_xml,
  'header|h=s'=>\$config_header,
);

#########################
# Chargement des règles #
#########################

# Le fichier de règles existe-t-il ?
if(! -e $config_rules) {
  die "Le fichier de règles $config_rules n'existe pas.\n";
}

# Log
if($config_verbose) {
  print STDERR "[info] Lecture des règles dans le fichier \"$config_rules\".\n";
}

open(my $fh, '<:encoding(UTF-8)', $config_rules) or die "Could not open file '$config_rules' $!\n";

my @rules = ();
my $tokensWindow = 0;
while(my $line = <$fh>) {  # Pour chaque ligne du fichier de règles
  if($line!~m/^\s*$/ && $line!~m/^\s*#/) {  # Si ce n'est pas une ligne vide ou un commentaire
    chomp $line;
    my $nRule = @rules;
    my ($left, $right) = split(/\s*->\s*/, $line);
    $right = $right || '';
    my @leftTokens = split(/\]\s+\[/, $left);
    my @rightTokens = split(/\]\s+\[/, $right);
    if($config_debug) {
      print STDERR "Règle n° $nRule\n";
      print STDERR "  Partie gauche: $left (".(@leftTokens+0)." tokens)\n";
    }

    # Ajuster la taille de la fenêtre pour l'application des règles
    if(@leftTokens > $tokensWindow) {
      $tokensWindow = @leftTokens;
    }

    # Traitement de la partie gauche
    for(my $nToken=0; $nToken<@leftTokens; ++$nToken) {
      while($leftTokens[$nToken]=~m/([a-zA-Z0-9_\@:-]+)(=|!=|>=?|<=?)(['"\/\|%])(.*?)\3/g) {
        my $tag = $1;
        my $relation = $2;
        my $type = $3;
        my $pattern = $4;
        if($config_debug) {
          print STDERR "    Tag=$tag Relation=$2 Type=$3 Pattern=$4\n";
        }
        $rules[$nRule]{'match'}[$nToken]{$tag}{'relation'} = $relation;
        $rules[$nRule]{'match'}[$nToken]{$tag}{'type'} = $type;
        $rules[$nRule]{'match'}[$nToken]{$tag}{'pattern'} = $pattern;
      }
      # Vérifier la syntaxe
      my $somethingleft = $leftTokens[$nToken]=~s/([a-zA-Z0-9_\@:-]+)(=|!=|>=?|<=?)(['"\/\|%])(.*?)\3//gr;
      if($somethingleft!~m/^[\[\]\s]*$/) {
        print STDERR "[warn] Erreur sur la règle n°$nRule, partie gauche, token n°$nToken: ".$leftTokens[$nToken]."\n";
      }
    }

    # Traitement de la partie droite
    if($right) {
      if($config_debug) {
        print STDERR "  Partie droite: $right (".(@rightTokens+0)." tokens)\n";
      }
      for(my $nToken=0; $nToken<@rightTokens; ++$nToken) {
        while($rightTokens[$nToken]=~m/([a-zA-Z0-9_\@:-]+)(=)(['"\/\|\{%])(.*?)(?:\3|\})/g) {
          my $tag = $1;
          my $relation = $2;
          my $type = $3;
          if($type eq "'") {
            $type = '"';
          }
          if($type eq "%") {
            $type = '/';
          }
          my $pattern = $4;
          if($config_debug) {
            print STDERR "    Tag=$tag Relation=$relation Type=$type Pattern=$pattern\n";
          }
          $rules[$nRule]{'replace'}[$nToken]{$tag}{'relation'} = $relation;
          $rules[$nRule]{'replace'}[$nToken]{$tag}{'type'} = $type;
          $rules[$nRule]{'replace'}[$nToken]{$tag}{'pattern'} = $pattern;
        }
        # Vérifier la syntaxe
        my $somethingleft = $rightTokens[$nToken]=~s/([a-zA-Z0-9_\@:-]+)(=)(['"\/\|\{%])(.*?)(?:\3|\})//gr;
        if($somethingleft!~m/^[\[\]\s]*$/) {
          print STDERR "[warn] Erreur sur la règle n°$nRule, partie droite, token n°$nToken: ".$rightTokens[$nToken]."\n";
        }
      }
    }

  }
}

close($fh);


##########################
# Application des règles #
##########################

# Log
if($config_verbose) {
  print STDERR "[info] Application des règles lexicales.\n";
}

# Traitement du header
my $line = '';
if($config_header) {
  $line = $config_header=~s/,/\t/gr;
}
else {
  $line = <STDIN>;
  chomp $line;
}
print "$line\n";
my @heads = split(/\t/, $line);

my @xmltagStack = ();  # Pour le mode XML
my @lines = ();  # Fenêtre de lignes à traiter (fenêtre de $tokensWindow tokens)
while($line = <STDIN>) {
  if($line!~m/^\s*$/ && $line!~m/^\s*#$/ && ($config_xml || $line!~m/^\s*</)) {  # Ne pas traiter les lignes vides, ou commençant par #, ou (si l'option XML n'est pas activée) les lignes comportant une balise
    chomp $line;
    my $nLine=@lines;

    # Bug sur les 0
    $line=~s/(^|\t)0(?=\t|$)/$1_0_/g;

    # Si option XML non activée: ignorer le XML des tokens qui contiennent du XML
    # En pratique, on SUPPRIME ce XML, il sera perdu (= n'apparaîtra pas dans la sortie) si une règle est activée
    my $normalisedLine = $line;
    if(!$config_xml) {
      $normalisedLine=~s/<[^>]+>//g;
    }
    
    # Mode XML et on est sur une balise
    if($config_xml) {
      if($line=~m/^\s*<([^!>\/ ]+)/) {  # Balise ouvrante
        my $tagName = $1;
        my $tagIndex = @xmltagStack;
        $xmltagStack[$tagIndex] = ();  # On ajoute la balise dans la pile (même si elle n'a pas d'attribut)
        while($line=~m/ ([a-zA-Z0-9:_-]+)="(.*?)"/g) {
          my $attrName = $1;
          my $attrValue = $2;
          $xmltagStack[$tagIndex]{"$tagName\@$attrName"} = $attrValue;
        }
      }
      my %actualTags = %{getNestedTags(\@xmltagStack)};
      foreach my $key (keys %actualTags) {
        $lines[$nLine]{$key} = $actualTags{$key};  # Ajouter les valeurs des "tags" des balises XML
        #print STDERR "  $key=".$actualTags{$key}."\n";
      }
      if($line=~m/^\s*<\s*\// || $line=~m/\/\s*>\s*$/) {  # Balise fermante
        pop(@xmltagStack);  # Enlever la dernière balise de la pile;
      }
    }

    # Décomposer la ligne
    my $j=0;
    foreach my $cel (split(/\t/, $normalisedLine)) {
      $lines[$nLine]{$heads[$j] || ''} = $cel;  # Attribuer des valeurs aux "tags" qui constituent la ligne
      ++$j;
    }

    # Entretien de la fenêtre de lignes à traiter
    while(@lines > $tokensWindow) {  # S'assurer qu'on ne garde pas trop de lignes
      my %oldLine = %{shift(@lines)};
      printLine(\%oldLine);
    }
    
    # Application de toutes les règles sur cette la fenêtre de lignes courante
    if($line!~m/^\s*</) {  # On n'applique pas sur les balises
      for(my $nRule=0; $nRule<@rules; ++$nRule) {
        my $nbTokensForThisRule = @{$rules[$nRule]{'match'}};
        my $ruleMatches = 0;  # Vaut vrai si la règle s'applique
        our @w = ();  # Variable accessible depuis les règles de remplacement 
        if(@lines >= $nbTokensForThisRule) {  # Si il y a assez de lignes pour pouvoir tester la règle
          $ruleMatches = 1;
          for(my $nToken=0; $nToken<$nbTokensForThisRule; ++$nToken) {  # Chercher si la règle matche
            if($ruleMatches) {
              foreach my $tag (keys %{$rules[$nRule]{'match'}[$nToken]}) {
                my $relation = $rules[$nRule]{'match'}[$nToken]{$tag}{'relation'};
                my $type = $rules[$nRule]{'match'}[$nToken]{$tag}{'type'};
                my $pattern = $rules[$nRule]{'match'}[$nToken]{$tag}{'pattern'};

                # Test de la règle
                if($relation eq '=' && $type eq '"') {
#print STDERR $lines[$nToken]{$tag} . " - $nToken - $tag - $line - $tokensWindow - ".($lines[0]{"pos"}." - ".$lines[1]{'pos'})."\n";
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} eq $pattern;
                }
                elsif($relation eq '=' && $type eq '/') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag}=~m/^$pattern$/;
                  $w[$nToken+1]{$tag}[1] = $1;  # Ça ne marche pas avec une boucle...
                  $w[$nToken+1]{$tag}[2] = $2;
                  $w[$nToken+1]{$tag}[3] = $3;
                  $w[$nToken+1]{$tag}[4] = $4;
                  $w[$nToken+1]{$tag}[5] = $5;
                  $w[$nToken+1]{$tag}[6] = $6;
                  $w[$nToken+1]{$tag}[7] = $7;
                  $w[$nToken+1]{$tag}[8] = $8;
                  $w[$nToken+1]{$tag}[9] = $9;
                  $w[$nToken+1]{$tag}[10] = $10;
                }
                elsif($relation eq '!=' && $type eq '"') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} ne $pattern;
                }
                elsif($relation eq '!=' && $type eq '/') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag}!~m/^$pattern$/;
                  $w[$nToken+1]{$tag}[1] = $1;  # Ça ne marche pas avec une boucle...
                  $w[$nToken+1]{$tag}[2] = $2;
                  $w[$nToken+1]{$tag}[3] = $3;
                  $w[$nToken+1]{$tag}[4] = $4;
                  $w[$nToken+1]{$tag}[5] = $5;
                  $w[$nToken+1]{$tag}[6] = $6;
                  $w[$nToken+1]{$tag}[7] = $7;
                  $w[$nToken+1]{$tag}[8] = $8;
                  $w[$nToken+1]{$tag}[9] = $9;
                  $w[$nToken+1]{$tag}[10] = $10;
                }
                elsif($relation eq '>' && $type eq '"') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} > $pattern;
                }
                elsif($relation eq '<' && $type eq '"') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} < $pattern;
                }
                elsif($relation eq '>=' && $type eq '"') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} >= $pattern;
                }
                elsif($relation eq '<=' && $type eq '"') {
                  $ruleMatches &&= $lines[$nToken]{$tag} && $lines[$nToken]{$tag} <= $pattern;
                }
                else {
                  if(!$config_quiet) {
                    print STDERR "[warn] Règle n°$nRule inapplicable.\n";
                    print STDERR "  Tag=$tag Relation=$relation Type=$type Pattern=$pattern\n";
                  }
                  $ruleMatches = 0;
                }

              }
            }

            if($ruleMatches) {  # Si on matche toujours...
              # Initialisation de la variable accessible depuis les règles de remplacement
              foreach my $lineTag (keys $lines[$nToken]) {  # Initialiser l'index 0
                $w[$nToken+1]{$lineTag}[0] = $lines[$nToken]{$lineTag} || '';
              }
            }

          }
        }
        
        my $nbTokensLeft = @{$rules[$nRule]{'match'}};
        my $nbTokensRight = 0;
        if($rules[$nRule]{'replace'}) {
          $nbTokensRight = @{$rules[$nRule]{'replace'}}; 
        }
                
        if($ruleMatches) {  # La règle matche
          if($config_debug) {
            print STDERR "[debug] La règle n°$nRule s'applique à :\n".(Dumper @lines)."\".\n";
          }
          if(! $rules[$nRule]{'replace'}) {  # Rien à remplacer: on supprime la ligne
          }
          else {  # Qqch à remplacer         
            for(my $nToken=0; $nToken<@{$rules[$nRule]{'replace'}}; ++$nToken) {  # Pour chaque nouveau token
              foreach my $tag (keys %{$rules[$nRule]{'replace'}[$nToken]}) {
                my $type = $rules[$nRule]{'replace'}[$nToken]{$tag}{'type'};
                my $pattern = $rules[$nRule]{'replace'}[$nToken]{$tag}{'pattern'};
                $lines[$nToken]{$tag} = $lines[$nToken]{$tag} || '';  # Le cas échéant, création du token
                if($config_debug) {
                  print STDERR "  $tag=".$lines[$nToken]{$tag}." -> ";
                }

                if($type eq '/') {  # Réécrire la variable accessible depuis les règles de remplacement
                  my $nTokenp1 = $nToken + 1;
                  $pattern = "'$pattern'";
                  $pattern=~s/\$([0-9]+)/'.\$w[$nTokenp1]{'$tag'}[$1].'/g;  # $1 -> $w[...]{...}[1]
                  $pattern=~s/\.\.+/./g;
                }

                if($type eq '{') {  # Réécrire la variable accessible depuis les règles de remplacement
                  my $nTokenp1 = $nToken + 1;
                  $pattern=~s/\$([0-9]+)/\$w[$nTokenp1]{'$tag'}[$1]/g;  # $1 -> $w[...]{...}[1]
                  $pattern=~s/\$w([0-9]+)_(.*?)_([0-9]+)/\$w[$1]{'$2'}[$3]/g;  # $w1_lemma_1 -> $w[1]{'lemma'}[1]
                }

                # Remplacement
                if($type eq '"') {
                  $lines[$nToken]{$tag} = $pattern;  # Effectuer le remplacement
                }
                elsif($type eq '/' && ($nToken >= $nbTokensLeft || $rules[$nRule]{'match'}[$nToken]{$tag}{'type'} eq '/')) {
                  my $matchPattern = $rules[$nRule]{'match'}[$nToken]{$tag}{'pattern'};
                  $lines[$nToken]{$tag}=~s/^$matchPattern$/$pattern/ee;  # Effectuer le remplacement
                }
                elsif($type eq '{' && ($nToken >= $nbTokensLeft || ! $rules[$nRule]{'match'}[$nToken]{$tag})) {  # Remplacement de la valeur d'un tag qu'on n'a pas eu à matcher (donc pas de pattern de match correspondant)
                  $lines[$nToken]{$tag} = eval($pattern);  # Effectuer le remplacement
                }
                elsif($type eq '{' && $rules[$nRule]{'match'}[$nToken]{$tag}{'type'} eq '/') {
                  my $matchPattern = $rules[$nRule]{'match'}[$nToken]{$tag}{'pattern'};
#print STDERR $pattern;
                  $lines[$nToken]{$tag}=~s/^$matchPattern$/$pattern/ee;  # Effectuer le remplacement
                }
                elsif($type eq '{' && $rules[$nRule]{'match'}[$nToken]{$tag}{'type'} eq '"') {
                  my $matchPattern = $rules[$nRule]{'match'}[$nToken]{$tag}{'pattern'};
                  $lines[$nToken]{$tag}=~s/^$matchPattern$/$pattern/ee;  # Effectuer le remplacement
                }
                else {
                  if(!$config_quiet) {
                    print STDERR "[warn] Règle n°$nRule inapplicable.\n";
                  }
                }

                if($config_debug) {
                  print STDERR "  $tag=".$lines[$nToken]{$tag}."\n";
                }
              }
            }
          }

          # Une règle s'est appliquée, vérifier le nombre de tokens en sortie
          while($nbTokensLeft > $nbTokensRight) {  # Il y a plus de tokens en entrée que de tokens en sortie
            pop @lines;  # Effacer le dernier token
            if($config_debug) {
              print STDERR "  Suppression de la ligne \"$line\".\n";
            }
            --$nbTokensLeft;
          }

        }

      }
    }

  }
  else {  # Ligne à ignorer: on la reproduit telle quelle
    print $line;
  }
}

# Écrire les dernières lignes
while(@lines > $tokensWindow) {
  my %oldLine = %{shift(@lines)};
  printLine(\%oldLine);
}


sub printLine {
  my %line = %{$_[0]};

  print $line{$heads[0]} || '';
  for(my $i=1; $i<@heads; ++$i) {
    print "\t";
    print $line{$heads[$i]} || '';
  }
  print "\n";
}


sub getNestedTags {
  my @nest = @{$_[0]};
  my %res = ();
 
  for(my $i=0; $i<@nest; ++$i) {
    foreach my $tagName (keys %{$nest[$i]}) {
      $res{$tagName} = $nest[$i]{$tagName};
    }
  }

  return \%res;
}
